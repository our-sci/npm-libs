import _ from 'lodash';
import Papa from 'papaparse';

function parseCsv(content) {
    const parsed = Papa.parse(content).data;
    const header = parsed[0];
    const res = {};

    header.forEach((h, colIdx) => {
        res[h] = parsed.filter((item, idx) => idx !== 0).map(r => r[colIdx]);
    });

    return res;
}

// firebase.initializeApp(config);

const CSV_SURVEY_URL = 'https://app.our-sci.net/api/survey/result/csv/';

export function getToken() {
    try {
        // console.log(JSON.parse(JSON.parse(localStorage.getItem('persist:primary')).auth).token);
        return JSON.parse(
            JSON.parse(localStorage.getItem('persist:primary')).auth
        ).token;
    } catch (error) {
        return null;
    }
}

export function storeToken(token) {
    localStorage.setItem(
        'persist:primary',
        JSON.stringify({
            auth: {
                token
            }
        })
    );
}

export function getShareCodeParam(url) {
    const params = new URL(url).searchParams;
    const shareCode = params.get('sharecode');
    return shareCode;
}

async function fetchCsv(
    { id, dateFrom = null, dateTo = null },
    key,
    useSharecode
) {
    console.log(document.location);
    console.log(useSharecode);

    try {
        const shareCode = getShareCodeParam(document.location);
        console.log(shareCode);

        let url = new URL(`${CSV_SURVEY_URL}${id}`);
        const params = {};

        if (useSharecode) {
            params.sharecode = shareCode;
        }

        console.log(url);

        if (dateFrom) {
            params.dateTo = dateTo;
        }
        if (dateTo) {
            params.dateFrom = dateFrom;
        }
        params.groupDelimiter = '/';

        // url.search = new URL();
        url.search = new URLSearchParams(params);

        let headers = {};
        if (getToken()) {
            headers = {
                'X-Authorization-Firebase': getToken()
            };
        }

        // if (useCache()) {
        //   const cached = localStorage.getItem(url);
        //   if (cached != null) {
        //     loaded[id] = JSON.parse(cached);
        //     return;
        //   }
        // }

        let r = null;

        r = await fetch(url, {
            // TODO: fix, current token is causing a 403
            headers
        }).catch(() => {
            console.error('token probably expired, retrying without');
            return fetch(url);
        });

        const data = await r.text();

        // localStorage.setItem(url, data);
        return parseCsv(data);
    } catch (error) {
        console.error(`error fetching: ${error}`);
    }
}

async function fetchCsvs(surveys, sharecodeSurveys) {
    const loaded = {};

    // const r = await Promise.all([
    //     _.map(surveys, ({ id, dateFrom = null, dateTo = null }, key) => {
    //         return (loaded[key] = fetchCsv(
    //             { id, dateFrom, dateTo },
    //             key,
    //             false
    //         ));
    //     }),
    //     _.map(
    //         sharecodeSurveys,
    //         ({ id, dateFrom = null, dateTo = null }, key) => {
    //             return (loaded[key] = fetchCsv(
    //                 { id, dateFrom, dateTo },
    //                 key,
    //                 true
    //             ));
    //         }
    //     )
    // ]);

    const surveysResults = await Promise.all(
        _.map(surveys, ({ id, dateFrom = null, dateTo = null }, key) => {
            return fetchCsv({ id, dateFrom, dateTo }, key, false);
        })
    );

    const sharecodeSurveysResults = await Promise.all(
        _.map(
            sharecodeSurveys,
            ({ id, dateFrom = null, dateTo = null }, key) => {
                return fetchCsv({ id, dateFrom, dateTo }, key, true);
            }
        )
    );

    Object.entries(surveys).forEach(([k, _], i) => {
        loaded[k] = surveysResults[i];
    });

    Object.entries(sharecodeSurveys).forEach(([k, _], i) => {
        loaded[k] = sharecodeSurveysResults[i];
    });

    console.log(loaded);

    return loaded;
}

(async () => {
    window.addEventListener(
        'message',
        async event => {
            const iframe = document.getElementById('child');

            if (event.data.type && event.data.type === 'REQUEST_FETCH_CSV') {
                const { ids, sharecodeIds } = event.data.payload;

                const loaded = await fetchCsvs(ids, sharecodeIds);
                console.log(loaded);

                iframe.contentWindow.postMessage(
                    {
                        type: 'RETURN_FETCH_CSV',
                        payload: loaded
                    },
                    event.origin
                );
            }
        },
        false
    );
})();
