import * as surveyUtils from '../../common/lib/survey';
import ui from '../../common-ui/lib/index';
import requestFetchCsv from './surveyLoader';

const dashboard = {
    sayHello() {
        console.log('hello from the dashboard');
    },
    info(title, description, icon) {
        ui.infoBox(title, description, icon);
    },
    progressBox(title, content, progress, progressText, icon) {
        ui.progressBox(title, content, progress, progressText, icon);
    },
    actionBox(title, content, action, icon, actionIcon, callback) {
        ui.actionBox(title, content, action, icon, actionIcon, callback);
    },
    genericBox(title, wide = false) {
        return ui.genericBox(title, wide);
    },
    fetchSurveys(surveys, sharecodeSurveys) {
        return requestFetchCsv(surveys, sharecodeSurveys);
    },
    style(primary, secondary, accent) {
        ui.style(primary, secondary, accent);
    },
    debug(title, data) {
        ui.debug(title, data);
    },
    lineChart(title, x, values, axis, wide = false) {
        ui.lineChart(title, x, values, axis, wide);
    },
    plotly(
        title = '',
        data = {},
        layout = {},
        config = { responsive: true },
        wide = false,
        element = null
    ) {
        return ui.plotly(title, data, layout, config, wide, element);
    },
    csvDownloadBox(title, data) {
        ui.csvDownloadBox(title, data);
    },
    selectBox(title, data, callback, wide = false, element = null) {
        return ui.selectBox(title, data, callback, wide, element);
    },
    sliderBox(
        title = '',
        min = 0,
        max = 100,
        defaultValue = 50,
        callback,
        wide
    ) {
        return ui.sliderBox(title, min, max, defaultValue, callback, wide);
    },
    table(tableData) {
        ui.table(tableData);
    },
    dataTable(title, data) {
        return ui.dataTableBox(title, data);
    },
    section(title) {
        ui.section(title);
    },
    showLoader() {
        ui.showLoader();
    },
    hideLoader() {
        ui.hideLoader();
    },
    joinSurveyOn(
        baseSurveyParam,
        baseSurveyJoinField,
        surveyToJoin,
        surveyToJoinField,
        prefix
    ) {
        return surveyUtils.joinSurveyOn(
            baseSurveyParam,
            baseSurveyJoinField,
            surveyToJoin,
            surveyToJoinField,
            prefix
        );
    },
    mergeSurveys(surveysParam) {
        return surveyUtils.mergeSurveys(surveysParam);
    }
};

export { dashboard };
