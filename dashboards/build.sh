#!/bin/bash

cd `dirname $0`

npm run defs
npm run compile
npm run compile-wrapper
npm version patch
npm publish --access public
cp ./dist/bundle.js ../public/v1/dashboard.js

echo "ready to commit and push"