const webpack = require("webpack");
const WebpackDevServer = require("webpack-dev-server");
const { config } = require("./config");



config.mode = 'development';
config.devtool = 'inline-sourcemap';

const compiler = webpack(config);



const options = {
  contentBase: "public",
  historyApiFallback: true,
  host: "127.0.0.1",
  stats: {
    colors: true
  }
};

const server = new WebpackDevServer(compiler, options);

server.listen(7654, "0.0.0.0", () => {
  console.log("Starting server on http://localhost:7654");
});
