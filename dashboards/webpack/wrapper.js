const path = require("path");
const webpack = require("webpack");

const config = {
  entry: {
    app: ["@babel/polyfill", path.join(__dirname, "..", "src", "wrapper.js")]
  },
  mode: "production",
  devtool: "inline-source-map",
  output: {
    path: path.resolve(__dirname, "..", "dist"),
    filename: "wrapper-compiled.js",
    libraryTarget: "var",
    library: "wrapper",
    libraryExport: "default"
  },
  target: "web",
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          "style-loader",
          {
            loader: "css-loader"
          }
        ]
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: ["url-loader?limit=10000&mimetype=application/font-woff"]
      },
      {
        test: /\.(ttf|eot|svg|png|jpg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: ["file-loader"]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      }
    ]
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
  ]
};

const compiler = webpack(config);
compiler.run((err, stats) => {
  if (err) {
    console.error(err.stack || err);
    if (err.details) {
      console.error(err.details);
      throw err;
    }
    return;
  }

  const info = stats.toJson();

  if (stats.hasErrors()) {
    console.log("\x1b[31m");
    info.errors.forEach(element => {
      console.error(element);
    });
    console.log("\x1b[0m");
  }

  if (stats.hasWarnings()) {
    console.log("\x1b[33m");
    info.warnings.forEach(element => {
      console.warn(element);
    });
    console.log("\x1b[0m");
  }
});
