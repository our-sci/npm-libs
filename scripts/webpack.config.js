const path = require('path');

export default () => ({
    mode: 'production',
    entry: './lib/index.js',
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'oursci-scripts.js',
        libraryTarget: 'umd',
        globalObject: 'this',
        library: 'oursci-scripts'
    },
    externals: {},
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /(node_modules|bower_components)/,
                use: 'babel-loader'
            }
        ]
    }
});
