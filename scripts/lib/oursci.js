const axios = require('axios');
const papaparse = require('papaparse');

module.exports = formId => new Promise((resolve, reject) => {
    axios
        .get(`https://app.our-sci.net/api/survey/result/csv/by-form-id/${formId}`)
        .then((response) => {
            const parsed = papaparse.parse(response.data).data;
            const header = parsed[0];
            const result = {};
            header.forEach((h, colIdx) => {
                result[h] = parsed.filter((item, idx) => idx !== 0).map(r => r[colIdx]);
            });
            resolve(result);
        })
        .catch((error) => {
            reject(error);
        });
});
