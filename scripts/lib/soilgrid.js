const axios = require('axios');

module.exports = (lat, lon) => new Promise((resolve, reject) => {
    axios
        .get(`https://rest.soilgrids.org/query?lon=${lon}&lat=${lat}`)
        .then((resp) => {
            resolve(resp.data);
        })
        .catch((error) => {
            console.log(error);
            reject(error);
        });
});
