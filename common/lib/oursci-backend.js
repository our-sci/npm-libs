const Papa = require('papaparse');

const fetchCsv = async ids => {
    const loaded = {};

    await Promise.all(
        ids.map(async id => {
            try {
                const url = `https://app.our-sci.net/api/survey/result/csv/${id}`;

                if (useCache()) {
                    const cached = localStorage.getItem(url);
                    if (cached != null) {
                        loaded[id] = cached;
                        return;
                    }
                }

                const r = await fetch(url);
                const text = await r.text();
                console.log('request done');
                localStorage.setItem(url, text);
                loaded[id] = text;
            } catch (error) {
                console.log(`error fetching: ${error}`);
            }
        })
    );

    return loaded;
};

function useCache() {
    const cache = localStorage.getItem(document.location.pathname + 'cache');

    if (cache === null) {
        localStorage.setItem(document.location.pathname + 'cache', false);
        return false;
    }

    return cache === 'true';
}

function parseCsv(id, content) {
    const parsed = Papa.parse(content[id]).data;
    const header = parsed[0];
    const res = {};

    header.forEach((h, colIdx) => {
        res[h] = parsed.filter((item, idx) => idx !== 0).map(r => r[colIdx]);
    });

    return res;
}

module.exports.loadSurvey = async (...ids) => {
    const loaded = await fetchCsv(ids);

    const results = {};
    ids.forEach(id => {
        results[id] = parseCsv(id, loaded);
    });

    return results;
};
