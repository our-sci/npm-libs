import _ from 'lodash';
import $ from 'jquery';
// import { transpose } from '../../dashboards/utils';

import infoBoxTemplate from '../template/info-box.handlebars';
import progressBoxTemplate from '../template/progress-box.handlebars';
import actionBoxTemplate from '../template/action-box.handlebars';
import genericBoxTemplate from '../template/generic-box.handlebars';
import tableTemplate from '../template/table.handlebars';
import sectionTemplate from '../template/section.handlebars';
import sliderTemplate from '../template/slider-box.handlebars';
import dataTableTemplate from '../template/datatable.handlebars';

export { genericBoxTemplate };

export function infoBox(title, content, icon) {
    const t = infoBoxTemplate({
        icon,
        title,
        content
    });

    var div = document.createElement('div');
    div.innerHTML = t;
    document.getElementById('container').appendChild(div.firstChild);
}

export function actionBox(title, content, action, icon, actionIcon, callback) {
    const t = actionBoxTemplate({ title, content, icon, action, actionIcon });

    var div = document.createElement('div');
    div.innerHTML = t;
    div.getElementsByClassName('action')[0].addEventListener('click', callback);
    div.querySelector('.info-bottom i').addEventListener('click', callback);
    document.getElementById('container').appendChild(div.firstChild);
}

export function progressBox(title, content, progress, progressText, icon) {
    const t = progressBoxTemplate({
        icon,
        title,
        content,
        progress,
        progressText
    });

    var div = document.createElement('div');
    div.innerHTML = t;
    document.getElementById('container').appendChild(div.firstChild);
}

export function table(table) {
    const t = tableTemplate({
        items: _.map(table, (v, k) => {
            const r = {};
            r['key'] = k;
            r['value'] = v;
            return r;
        })
    });

    var div = document.createElement('div');
    div.innerHTML = t;
    document.getElementById('container').appendChild(div.firstChild);
}

export function section(title) {
    const t = sectionTemplate({
        title
    });

    var div = document.createElement('div');
    div.innerHTML = t;
    document.getElementById('container').appendChild(div.firstChild);
}

export function sliderBox(
    title,
    min,
    max,
    defaultValue,
    callback,
    wide = false
) {
    const classes = wide ? 'col-lg-12' : 'col-lg-6';

    const t = sliderTemplate({
        classes,
        title,
        min,
        max,
        defaultValue
    });

    const div = document.createElement('div');
    div.innerHTML = t;
    div.getElementsByClassName('slider')[0].oninput = function() {
        callback(this.value);
    };
    return document.getElementById('container').appendChild(div.firstChild);
}

export function genericBox(title, wide = false) {
    const classes = wide ? 'col-lg-12' : 'col-lg-6';

    const t = genericBoxTemplate({
        classes,
        title
    });

    const div = document.createElement('div');
    div.innerHTML = t;
    return document.getElementById('container').appendChild(div.firstChild);
}

let dataTableCounter = 0;
/**
 *
 * @param {*} title Title of the box
 * @param {*} data Survey formatted data
 */
export function dataTableBox(title, data) {
    function transpose(a) {
        const arr = Object.values(a);
        return arr[0].map(function(_, c) {
            return arr.map(function(r) {
                return r[c] ? r[c] + '' : '';
            });
        });
    }

    const t = dataTableTemplate({
        title
    });

    var div = document.createElement('div');
    div.innerHTML = t;

    let columns = Object.keys(data).map(k => {
        return {
            title: k + ''
        };
    });

    const transposed = transpose(data);
    const table = $('table', div.firstChild);
    
    table.attr('id', `dataTable-${dataTableCounter++}`);
    
    table.dataTable({
        data: transposed,
        columns: columns,
        scrollX: true
    });
    
    return document.getElementById('container').appendChild(div.firstChild);
}
