import $ from 'jquery';
import 'select2';

import styleTemplate from '../template/style.handlebars';
import _ from 'lodash';
import Papa from 'papaparse';
import JSONFormatter from 'json-formatter-js';
export * from './charting';
import * as boxes from './boxes';
export * from './boxes';

export function style(primary, secondary, accent) {
    const t = styleTemplate({
        primary,
        secondary,
        accent
    });

    var s = document.createElement('div');
    s.type = 'text/css';
    s.innerHTML = t;
    document.head.appendChild(s.firstChild);
}

export function debug(title, data) {
    const formatter = new JSONFormatter(data);
    const content = formatter.render();

    const t = boxes.genericBoxTemplate({
        supClass: 'col-lg-6',
        title
    });

    var div = document.createElement('div');
    div.innerHTML = t;
    div.getElementsByClassName('target')[0].appendChild(content);
    div.getElementsByClassName('target')[0].classList.add('card-pad');
    document.getElementById('container').appendChild(div.firstChild);
}

function surveyToCSV(survey) {
    const arr = _.values(survey);

    const input = {
        fields: _.keys(survey),
        data: _.zip(...arr)
    };
    return Papa.unparse(input);
}

function downloadCsv(title, data) {
    const payload = surveyToCSV(data);
    const a = document.createElement('a');
    document.body.appendChild(a);   
    a.style = 'display: none';
    const blob = new Blob([payload], {
        type: 'text/csv'
    });
    const url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = title + '.csv';
    a.click();
    window.URL.revokeObjectURL(url);
}

export function csvDownloadBox(title, data) {
    boxes.actionBox(
        title,
        `Download ${_.keys(data).length} columns, ${
            _.values(data)[0].length
        } rows`,
        'Download as CSV',
        'fa-download',
        'fa-download',
        () => {
            downloadCsv(title, data);
        }
    );
}

export function showLoader() {
    const html =
        '<div class="page-center"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>';
    var div = document.createElement('div');
    div.innerHTML = html;
    document.body.appendChild(div.firstChild);
}

export function hideLoader() {
    const c = document.getElementsByClassName('page-center');
    if (!c) {
        return;
    }

    _.forEach(c, e => {
        e.remove();
    });
}

/**
 * create select box, we need jquery for the select2 library
 * @param {*} title
 * @param {*} data
 * @param {*} callback
 * @param {*} wide
 * @param {*} element
 */
export function selectBox(title, data, callback, wide, element) {
    const select = $('<select></select>');
    select.append($('<option></option>'));
    const box = element ? $(element) : $(boxes.genericBox(title, wide));
    $('.target', box).addClass('content-pad');
    $('.target', box).append(select);
    $('.target', box).css('min-height', 'initial');
    $('.target', box).css('padding-bottom', '1em');
    select.css('width', '100%');
    select.css('margin-bottom', '0.5em');

    select.select2({
        placeholder: 'Select an option',
        allowClear: true,
        data
    });

    select.on('select2:select', e => {
        callback(e.params.data, box);
    });
    return box;
}
