export function filter(obj, cb) {
    const len = Object.values(obj)[0].length;
    const keys = Object.keys(obj);

    const indices = [];

    for (let i = 0; i < len; i += 1) {
        const cur = {};
        keys.forEach(key => {
            cur[key] = obj[key][i];
        });

        if (cb(cur) === true) {
            indices.push(i);
        }
    }

    const resultSet = {};
    Object.keys(obj).forEach(key => {
        const values = obj[key].filter((_, idx) => indices.includes(idx));

        resultSet[key] = values;
    });

    return resultSet;
}

export function zip(obj) {
    const results = [];

    const len = Object.values(obj)[0].length;
    const keys = Object.keys(obj);

    for (let i = 0; i < len; i += 1) {
        const cur = {};
        keys.forEach(key => {
            cur[key] = obj[key][i];
        });

        results.push(cur);
    }

    return results;
}

export function countDistinct(obj, field) {
    return zip(obj).reduce((prev, next) => {
        const ret = prev;

        if (!next[field]) {
            return prev;
        }

        if (typeof prev[next[field]] === 'undefined') {
            ret[next[field]] = 0;
        }

        ret[next[field]] += 1;
        return ret;
    }, {});
}

export function distinct(obj) {
    return obj.filter((v, i, a) => a.indexOf(v) === i).filter(v => v);
}

export function count(obj) {
    return Object.values(obj)[0].length;
}

export function countIfAnswered(obj, field) {
    let cnt = 0;
    obj[field].forEach(item => {
        if (item) {
            cnt += 1;
        }
    });

    return cnt;
}

/**
export function param(parameter, defaultvalue) {
    return (
        url.parse(window.location.href, true).query[parameter] || defaultvalue
    );
}
**/

export function groupBy(obj, field) {
    return distinct(obj[field]).map(treeId =>
        filter(obj, row => row[field] === treeId)
    );
}

export function transpose(a) {
    const arr = Object.values(a);
    return arr[0].map(function(_, c) {
        return arr.map(function(r) {
            return r[c] ? r[c] + '' : '';
        });
    });
}
