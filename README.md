# NPM repository for Our-Sci

This repository contains the source code for all Javascript related functionality for both the web application with dashboards and the Android application with measurement scripts and processor scripts.

## ./common

Common code is shared between all libraries. For instance the API describing how to fetch data from a survey or csv is found in here, as it is applied accross multiple platforms.

## ./common-ui

Code here is shared between the Android application for showing figures / diagrams and the dashboards on the web application.

## dashboards

The js code for dashboards is found in here. A dashboard itself can be written as js module and include the loader which is found in `public/v1/loader.js`. While the dashboard library is compiled and packed via `webpack` its dependencies are often dynamically loaded at startup via CDN.