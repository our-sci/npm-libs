#!/usr/bin/env node

const fs = require('fs');

const packProcessorDevServer = require('./pack-processor-dev-server');
const packProcessor = require('./pack-processor');
const packSensor = require('./pack-sensor');
const packProject = require('./pack-project');
const deploy = require('./deploy');
const upload = require('./upload');
const auth = require('./auth');

const bundlerActions = {
	'run-processor-dev': {
		description: 'Start the processor script in a development server.',
		execute: packProcessorDevServer.run,
	},
	'build-processor-browser': {
		description: 'Build the processor script bundled for the browser.',
		execute: () => packProcessor.packBrowser(() => console.log('done')),
	},
	'build-processor': {
		description: 'Build the processor script bundled for Android',
		execute: packProcessor.pack,
	},
	'build-sensor': {
		description: 'Build the sensor script',
		execute: packSensor.run,
	},
	'build-project': {
		description: 'Build the measurement script project: build processor and sensor script and add to zip archive',
		execute: packProject.run,
	},
	'deploy': {
		description: 'Deploy built measurement script to Android device',
		execute: deploy.run,
	},
	// 'sensor': {
	// 	description: 'Execute the sensor script',
	// 	execute: () => execFile('babel-node --presets=@babel/preset-env src/sensor.js')
	// },
	// 'run-sensor-build-processor': {
	// 	description: 'Run the sensor script then build the processor script for browser',
	// 	execute: () => execFile('run-s sensor build-processor-browser')
	// },
	'build-and-deploy': {
		description: 'Build the measurement script project then deploy it to an Android device',
		execute: () => packProject.run(() => deploy.run()),
	},
	'upload': {
		description: 'Upload the built measurement script bundle to OurSci platform',
		execute: upload.run,
	},
	'auth': {
		description: 'Authenticate with OurSci platform',
		execute: auth.run,
	},
	'compile-upload': {
		description: 'Build the sensor and processor scripts, then upload bundle to OurSci platform',
		execute: () => packSensor.pack(() => packProcessor.pack(() => upload.run())),
	},
};

if (process.argv.length < 3) {
	console.log('Usage: oursci-measurement-script-bundler {action}');
	const actionDescriptions = Object.entries(bundlerActions)
		.map(([k, v]) => `${k}: ${v.description}`)
		.join('\n\t');
	console.log(`Available actions: \n\t${actionDescriptions}`);
} else {
	const targetAction = process.argv.slice(2);
	bundlerActions[targetAction].execute();
}